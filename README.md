# README #

This is a free version of a simple hotel management system for freepbx 14 it has not been tested against other version.

the load script for the database is supplied but you will need to set user and passwords.

This is supplied as is. do not email expecting free support and assistance on installation.

===

To Install copy the files to a directory freehms in you root web directory.

Copy the contents of the extenions_custom.conf to /etc/asterisk/extenions_custom.conf

Now make sure loadmysql.sh is executable and run it, This will create the required database in mysql. if you have a root mysql password you will be promted for it
NOTE when it is confirmed all is working delete or move this file from this directory.

Finally you need to set the mysql passwords in config.inc.php , This file also lets you customise the language of the system including the Currency symbol and any sales tax. All labels are derived from here

The User access levels are set in FreePBX "User Management" create 3 users Admin, Reception and HouseKeeping or call teh users what ever you wish . in their 'Department' enter 'admin' 'reception' and 'housekeeping' *Note these are case sensitive, But these can be changed in the Code if you wish. They log into the system with the the user and password defined here. You may want to give users different accounts as this will let you keep a log of who was logged in when by looking at /var/log/asterisk/freepbx_security.log 

