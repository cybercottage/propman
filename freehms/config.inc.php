<?php
//copywrite
$str = 'JmNvcHk7IDIwMTYgY3liZXItY290dGFnZS5jby51ayAmbmJzcDsmbmJzcDs=';

//mysql database for users , normally freepbx

define("DB_HOST", "localhost");
define("DB_NAME", "asterisk");
define("DB_USER", "freepbxuser");
define("DB_PASS", "yoursecret");

//mysql database connection settings

//Hotel database
$dbhost = "localhost";
$dbpass = "";
$dbuser = "root";
$dbname = "Hotel";


//Asterisk database
$dbhost2 = "localhost";
$dbpass2 = "yoursecret";
$dbuser2 = "freepbxuser";
$dbname2 = "asterisk";

//Asteriskcdrdb database
$dbhost3 = "localhost";
$dbpass3 = "yoursecret";
$dbuser3 = "freepbxuser";
$dbname3 = "asteriskcdrdb";

$custchan = "outbound-allroutes";

//Label
$Title = "FreePBX | HotBill";
$MenuTop1 = "Home";
$MenuTop2 = "Rooms";
$MenuTop3 = "Rates";
$MenuTop4 = "Check In";
$MenuTop5 = "Check out";
$MenuTop6 = "Bills";
$MenuTop7 = "Alarm";
$MenuTop9 = "Logout";
$MenuTop10 = "Clean Rooms";
$MenuTop11 = "Rooms to Clean";
$Title1 = "FreePBX Call Management";
$Title2 = "Phone management";
$Title3 = "Rate management";
$Title4 = "Phone Check-in";
$Title5 = "Phone Check-out";
$Title6 = "Phone Billing";
$Title7 = "Alarm Service";
$Title8 = "Call Details";
$Title9 = "Logout";
$Title10 = "Clean Rooms";
$Title11 = "Rooms to be cleaned";

$SecLab1 = "Action";
$SecLab2 = "Insert";
$SecLab3 = "Delete";
$SecLab4 = "Extensions imported from Freepbx";
$SecLab5 = "Import from Freepbx";
$SecLab6 = "Ext";
$SecLab7 = "Phone Name";
$SecLab8 = "Description";
$SecLab9 = "Type";
$SecLab10 = "Prefix";
$SecLab11 = "Minute rate";
$SecLab12 = "Fixed rate";
$SecLab13 = "Insert date";
$SecLab14 = "Name";
$SecLab15 = "Check In";
$SecLab16 = "Update";
$SecLab17 = "Check Out";
$SecLab18 = "from dd/mm/yyyy";
$SecLab19 = "to dd/mm/yyyy";
$SecLab20 = "Rooms";
$SecLab21 = "Search";
$SecLab22 = "All";
$SecLab23 = "View";
$SecLab24 = "Time";
$SecLab25 = "Call date";
$SecLab26 = "Destination";
$SecLab27 = "Secs";
$SecLab28 = "Amount";
$SecLab29 = "Report";
$SecLab30 = "Add new Alarm";
$SecLab31 = "Total@Checkout";
$SecLab32 = "Total Inc Tax";
$SecLab33 = "Net Total";
$SecLab34 = "Calls rounded to nearest ";
$SecLab35 = "To Clean";
$SecLab36 = "Cleaned";
$SecLab37 = "Print Cleaning List";
$SecLab38 = "Print Bill";
$currency = "Penny";
$currencysymbol = "£";
$vat = "20";
?>
